# TODO improve artifacts sharing between the jobs, keep track of the following issues:
# - https://github.com/travis-ci/travis-ci/issues/7590
# - https://travis-ci.community/t/using-unified-cache-control-cache-identity/1531

branches:
  only:
    - master

node_js: 12
language: node_js

embedding-prepare-webclients-mail-settings: &embedding-prepare-webclients-mail-settings-anchor
  cache:
    yarn: false
    npm: false
    directories: [ './output/git/backup/proton-mail-settings' ]
  env:
    - ELECTRON_MAIL_CI_PROTON_CLIENTS_ONLY="proton-mail-settings"
  script: ./scripts/ci/prepare-webclients.sh

embedding-prepare-webclients-contacts: &embedding-prepare-webclients-contacts-anchor
  cache:
    yarn: false
    npm: false
    directories: [ './output/git/backup/proton-contacts' ]
  env:
    - ELECTRON_MAIL_CI_PROTON_CLIENTS_ONLY="proton-contacts"
  script: ./scripts/ci/prepare-webclients.sh

embedding-prepare-webclients-calendar: &embedding-prepare-webclients-calendar-anchor
  cache:
    yarn: false
    npm: false
    directories: [ './output/git/backup/proton-calendar' ]
  env:
    - ELECTRON_MAIL_CI_PROTON_CLIENTS_ONLY="proton-calendar"
  script: ./scripts/ci/prepare-webclients.sh

embedding-prepare-webclients-mail: &embedding-prepare-webclients-mail-anchor
  cache:
    yarn: false
    npm: false
    directories: [ './output/git/backup/proton-mail' ]
  env:
    - ELECTRON_MAIL_CI_PROTON_CLIENTS_ONLY="proton-mail"
  script: ./scripts/ci/prepare-webclients.sh

embedding-prepare-webclients-account: &embedding-prepare-webclients-account-anchor
  cache:
    yarn: false
    npm: false
    directories: [ './output/git/backup/proton-account' ]
  env:
    - ELECTRON_MAIL_CI_PROTON_CLIENTS_ONLY="proton-account"
  script: ./scripts/ci/prepare-webclients.sh

embedding-linux-common: &embedding-linux-common-anchor
  os: linux
  dist: bionic
  env:
    # needed for: native modules compiling
    - CC=gcc-7 CXX=g++-7
  addons:
    apt:
      packages:
        # needed for: native modules compiling
        - g++-7
        # needed for: compiling "desktop-idle" native module
        - libxss-dev
        # needed for: keychain initialization and compiling "node-keytar" native module
        - gnome-keyring
        - libgnome-keyring-dev
        - libsecret-1-dev
        # needed for: keychain access testing before running e2e tests (see "scripts/ci/add-secret-service-item-assert.py")
        - python3-secretstorage
        # needed for: tweaking snap package ("unsquashfs" binary)
        - squashfs-tools

embedding-macos-common: &embedding-macos-common-anchor
  os: osx
  osx_image: xcode9.4

embedding-build-app-common: &embedding-build-app-common-anchor
  cache: false
  after_failure:
    - tar -cvf e2e-logs.tar ./output/e2e
    - yarn scripts/transfer upload e2e-logs.tar

before_install:
  - | # installing the most recent yarn version
    curl -o- -L https://yarnpkg.com/install.sh | bash;
    export PATH="$HOME/.yarn/bin:$PATH";
install:
  - node --version
  - npm --version
  - yarn --version
  - npx envinfo
  - yarn install --pure-lockfile
notifications:
  email:
    on_success: never
    on_failure: change

jobs:
  include:
    # pre-building proton web clients in individual jobs to workaround the issue of travis dropping long-running jobs on free plan
    # [mail-settings]
    - stage: 'build proton clients'
      <<: *embedding-linux-common-anchor
      <<: *embedding-prepare-webclients-mail-settings-anchor
      workspaces:
        create:
          name: 'linux-webclients-artifact-mail-settings'
          paths: [ './output/git/backup/proton-mail-settings' ]
    - stage: 'build proton clients'
      <<: *embedding-macos-common-anchor
      <<: *embedding-prepare-webclients-mail-settings-anchor
      workspaces:
        create:
          name: 'macos-webclients-artifact-mail-settings'
          paths: [ './output/git/backup/proton-mail-settings' ]
    # [contacts]
    - stage: 'build proton clients'
      <<: *embedding-linux-common-anchor
      <<: *embedding-prepare-webclients-contacts-anchor
      workspaces:
        create:
          name: 'linux-webclients-artifact-contacts'
          paths: [ './output/git/backup/proton-contacts' ]
    - stage: 'build proton clients'
      <<: *embedding-macos-common-anchor
      <<: *embedding-prepare-webclients-contacts-anchor
      workspaces:
        create:
          name: 'macos-webclients-artifact-contacts'
          paths: [ './output/git/backup/proton-contacts' ]
    # [calendar]
    - stage: 'build proton clients'
      <<: *embedding-linux-common-anchor
      <<: *embedding-prepare-webclients-calendar-anchor
      workspaces:
        create:
          name: 'linux-webclients-artifact-calendar'
          paths: [ './output/git/backup/proton-calendar' ]
    - stage: 'build proton clients'
      <<: *embedding-macos-common-anchor
      <<: *embedding-prepare-webclients-calendar-anchor
      workspaces:
        create:
          name: 'macos-webclients-artifact-calendar'
          paths: [ './output/git/backup/proton-calendar' ]
    # [proton-mail]
    - stage: 'build proton clients'
      <<: *embedding-linux-common-anchor
      <<: *embedding-prepare-webclients-mail-anchor
      workspaces:
        create:
          name: 'linux-webclients-artifact-mail'
          paths: [ './output/git/backup/proton-mail' ]
    - stage: 'build proton clients'
      <<: *embedding-macos-common-anchor
      <<: *embedding-prepare-webclients-mail-anchor
      workspaces:
        create:
          name: 'macos-webclients-artifact-mail'
          paths: [ './output/git/backup/proton-mail' ]
    # [proton-account]
    - stage: 'build proton clients'
      <<: *embedding-linux-common-anchor
      <<: *embedding-prepare-webclients-account-anchor
      workspaces:
        create:
          name: 'linux-webclients-artifact-account'
          paths: [ './output/git/backup/proton-account' ]
    - stage: 'build proton clients'
      <<: *embedding-macos-common-anchor
      <<: *embedding-prepare-webclients-account-anchor
      workspaces:
        create:
          name: 'macos-webclients-artifact-account'
          paths: [ './output/git/backup/proton-account' ]
    # [build]
    - stage: 'build app'
      <<: *embedding-linux-common-anchor
      <<: *embedding-build-app-common-anchor
      workspaces:
        use:
          - 'linux-webclients-artifact-mail-settings'
          - 'linux-webclients-artifact-contacts'
          - 'linux-webclients-artifact-calendar'
          - 'linux-webclients-artifact-mail'
          - 'linux-webclients-artifact-account'
      sudo: required
      services:
        - docker
        # needed for: running e2e tests
        - xvfb
      before_script:
        - | # init dbus
          NO_AT_BRIDGE=1;
          eval $(dbus-launch --sh-syntax);
        # TODO get back keyring initialization then enable respective e2e test case (see "src/e2e/index.spec.ts")
        #- | # init keychain
        #  eval $(echo -n "" | /usr/bin/gnome-keyring-daemon --login);
        #  eval $(/usr/bin/gnome-keyring-daemon --components=secrets --start);
        #  python3 ./scripts/ci/add-secret-service-item-assert.py;
      script: ./scripts/ci/travis/build-linux.sh
    - stage: 'build app'
      <<: *embedding-macos-common-anchor
      <<: *embedding-build-app-common-anchor
      workspaces:
        use:
          - 'macos-webclients-artifact-mail-settings'
          - 'macos-webclients-artifact-contacts'
          - 'macos-webclients-artifact-calendar'
          - 'macos-webclients-artifact-mail'
          - 'macos-webclients-artifact-account'
      env: [ { ARTIFACT_NAME_POSTFIX: "-high-sierra" } ]
      script: ./scripts/ci/travis/build-osx.sh
    - stage: 'build app'
      <<: *embedding-macos-common-anchor
      <<: *embedding-build-app-common-anchor
      osx_image: xcode11.3
      workspaces:
        use:
          - 'macos-webclients-artifact-mail-settings'
          - 'macos-webclients-artifact-contacts'
          - 'macos-webclients-artifact-calendar'
          - 'macos-webclients-artifact-mail'
          - 'macos-webclients-artifact-account'
      env: [ { ARTIFACT_NAME_POSTFIX: "-mojave" } ]
      script: ./scripts/ci/travis/build-osx.sh
